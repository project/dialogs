<?php

namespace Drupal\dialogs;

use Drupal\Core\Render\BubbleableMetadata;
use Drupal\Core\Render\Renderer;

class Dialogifyer {

  /**
   * The dialog type.
   *
   * @var string
   */
  protected $dialogType;

  /**
   * The dialog renderer.
   *
   * @var string
   */
  protected $dialogRenderer;

  /**
   * The dialog options.
   *
   * @var array|string|null
   */
  protected $dialogOptions;

  /**
   * @var array
   */
  protected $libraries;

  /**
   * @var array
   */
  protected $queryParametersToUnset;

  /**
   * The renderer service.
   *
   * @var \Drupal\Core\Render\Renderer
   */
  protected $renderer;

  /**
   * LinkDialogifyer constructor.
   *
   * @param string $dialogType
   *   The type.
   * @param string $dialogRenderer
   *   The renderer.
   * @param array|string|null $dialogOptions
   *   The options.
   * @param array $libraries
   *   The effect libraries to load.
   * @param array $queryParametersToUnset
   *   The query parameters to unset.
   * @param \Drupal\Core\Render\Renderer $renderer
   *   The renderer service.
   */
  public function __construct($dialogType, $dialogRenderer, $dialogOptions, $libraries, $queryParametersToUnset, Renderer $renderer) {
    $this->dialogType = $dialogType;
    $this->dialogRenderer = $dialogRenderer;
    $this->dialogOptions = $dialogOptions;
    $this->libraries = $libraries;
    $this->queryParametersToUnset = $queryParametersToUnset;
    $this->renderer = $renderer;
  }

  /**
   * Alter the link options.
   *
   * @param array $linkOptions
   *   The link options.
   * @param \Drupal\Core\Render\BubbleableMetadata|NULL $bubbleable_metadata
   *   The metadata. If null, metadata goes to render context if exists.
   *
   * @throws \Exception
   */
  public function alterLinkOptions(array &$linkOptions, BubbleableMetadata $bubbleable_metadata = NULL) {
    $linkOptions['attributes']['class'][] = 'use-ajax';
    $linkOptions['attributes']['data-dialog-type'] = $this->dialogType;
    if ($this->dialogRenderer) {
      $linkOptions['attributes']['data-dialog-renderer'] = $this->dialogRenderer;
    }
    if ($this->dialogOptions) {
      if (is_array($this->dialogOptions)) {
        $linkOptions['attributes']['data-dialog-options'] = json_encode($this->dialogOptions);
      }
      else {
        $linkOptions['attributes']['data-dialog-options'] = $this->dialogOptions;
      }
    }

    // If empty destination, add current path, for forms.
    if (isset($linkOptions['query']['destination']) && empty($linkOptions['query']['destination'])) {
      $linkOptions['query']['destination'] = \Drupal::destination()->get();
    }

    foreach ($this->queryParametersToUnset as $queryParameter) {
      unset($linkOptions['query'][$queryParameter]);
    }

    $libraries = array_merge(['core/drupal.dialog.ajax'], $this->libraries);

    if ($bubbleable_metadata) {
      $bubbleable_metadata->addAttachments(['library' => $libraries]);
    }
    elseif ($this->renderer->hasRenderContext()) {
      $build = ['#attached' => ['library' => $libraries]];
      $this->renderer->render($build);
    }
  }

}
